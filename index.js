// var sayHello = "Hello World!"
// console.log(sayHello)

// // let angka 
// // angka = 10

// let angka
// angka = 100
// console.log(angka == 100)
// console.log(angka == 20)

// let sifat
// sifat = "rajin"
// console.log(sifat != "malas")
// console.log(sifat != "bandel")

// let angka1 = 8
// console.log(angka1 == "8")
// console.log(angka1 === "8")
// console.log(angka1 === 8)

// let angka2 = 11
// console.log(angka != "11")
// console.log(angka2 !== "11")
// console.log(angka2 !== 11)

// let number = 17
// console.log(number < 20)
// console.log(number > 17)
// console.log(number >= 17)
// console.log(number <= 20)

// console.log(true || true)
// console.log(true || false)
// console.log(true || false || false)
// console.log(false || false)


// console.log(true && true)
// console.log(true && false)
// console.log(false && false)
// console.log(false && true && true)
// console.log(true && true && true)

// if (true) {
//     console.log("jalankan code")
// } else if (false) {
//     console.log("code tidak dijalankan")
// }

// var mood = "happy"
// if (mood = "happy") {
//     console.log("hari ini akau bahagia")
// }

// var minimarketStatus = "open"
// if (minimarketStatus == "open") {
//     console.log("Saya akan membeli telur dan buah")
// } else {
//     console.log("Saya akan kembali lagi besok")
// }

// var minimarketStatus = "close"
// var minuteRemainingToOpen = 5
// if (minimarketStatus == "open") {
//     console.log("saya akan membeli telur dan buah")
// } else if (minuteRemainingToOpen <= 5) {
//     console.log("minimarket buka sebentar lagi, saya tungguin")
// } else {
//     console.log("minimarket tutup, saya pulang lagi")
// }

// var minimarketStatus = "open"
// var telur = "soldout"
// var buah = "soldout"
// if (minimarketStatus == "open") {
//     console.log("saya akan membeli telur dan buah")
//     if (telur == "soldout" || buah == "soldout") {
//         console.log("belanjaan saya tidak lengkap")
//     } else if (telur == "soldout") {
//         console.log("telur habis")
//     } else if (buah == "soldout") {
//         console.log("buah habis")
//     }
// } else {
//     console.log("minimarket tutup, saya pulang lagi")
// }

// var buttonPushed = 3;
// switch (buttonPushed) {
//     case 1: { console.log('matikan TV!'); break; }
//     case 2: { console.log('turunkan volume TV!'); break; }
//     case 3: { console.log('tingkatkan volume TV!'); break; }
//     case 4: { console.log('matikan suara TV!'); break; }
//     default: { console.log('Tidak terjadi apa-apa'); }
// }

// var age = 20
// var bisaVote = age > 20 ? "bisa vote" : "belum bisa vote"

// var angka = 9
// var jenisBilangan = angka % 2 === 0 ? "Bilangan Genap" : "Bukan Bilangan Genap"

for (var angka = 1; angka < 10; angka++) {
    console.log('Iterasi ke-' + angka);
}

var jumlah = 0;

for (var deret = 5; deret > 0; deret--) {
    jumlah += deret;
    console.log('Jumlah saat ini: ' + jumlah);
}

console.log('Jumlah terakhir: ' + jumlah);

for (var deret = 0; deret < 10; deret += 2) {
    console.log('Iterasi dengan Increment counter 2: ' + deret);
}

console.log('-------------------------------');

for (var deret = 15; deret > 0; deret -= 3) {
    console.log('Iterasi dengan Decrement counter : ' + deret);
}

for (var i = 0; i <= 6; i++) {
    if (i === 3) {
        console.log("ini For-Loop dengan Kondisi")
    } else {
        console.log(i)
    }
}

var flag = 1;
while (flag < 10) {
    console.log('Iterasi ke-' + flag);
    flag++;
}

var deret = 5;
var jumlah = 0;
while (deret > 0) {
    jumlah += deret;
    deret--;
    console.log('Jumlah saat ini: ' + jumlah)
}

var i = 0;

while (i < 5) {

    if (i === 3) {

        console.log("Ini While dengan Kondisi")
    } else {

        console.log(i)
    }
    i++;
}

var hobbies = ["coding", "cycling", "climbing", "skateboarding"]
console.log(hobbies) 
console.log(hobbies.length) 

console.log(hobbies[0]) 
console.log(hobbies[2]) 
console.log(hobbies[hobbies.length - 1])