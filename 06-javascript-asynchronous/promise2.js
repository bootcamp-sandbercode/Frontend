function filterBooksPromise(colorful, amountOfPage) {
    return new Promise(function (resolve, reject) {
        var books = [
            { name: "Shinchan", totalPage: 50, isColorful: true },
            { name: "Kalkulus", totalPage: 250, isColorful: false },
            { name: "Doraemon", totalPage: 40, isColorful: true },
            { name: "Algoritma", totalPage: 250, isColorful: false },
        ]
        if (amountOfPage >= 40) {
            resolve(books.filter(x => x.totalPage >= amountOfPage && x.isColorful == colorful));
        } else {
            var reason = "Maaf buku di bawah 40 halaman tidak tersedia"
            reject(reason);
        }
    });
}

const execute = async (x, y) => { // x is param for colorful & y is param for amountOfPage
    try {
        let result = await filterBooksPromise(x, y);
        console.log(result);
    } catch (error) {
        console.log(error);
    }
}

execute(true, 40);
execute(false, 250);
execute(true, 30);