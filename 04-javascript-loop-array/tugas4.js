// Konstanta
let def = 2;

// Soal 1
console.log(">==========Soal 1==========<");

for (var i = 0; i < 10; i++) {
    console.log(i);
}

// Soal 2
console.log(">==========Soal 2==========<");

for (var i = 1; i < 10; i++) {
    if (i % def != 0) {
        console.log(i);
    }
}

// Soal 3
console.log(">==========Soal 3==========<");

for (var i = 1; i < 10; i++) {
    if (i % def == 0) {
        console.log(i);
    }
}

// Soal 4
console.log(">==========Soal 4==========<");

let array1 = [5, 2, 4, 1, 3, 5];

index5 = array1[5];
console.log(index5);

// Soal 5
console.log(">==========Soal 5==========<");

let array2 = [5, 2, 4, 1, 3, 5];

result = array1.sort();
console.log(result);

// Soal 6
console.log(">==========Soal 6==========<");

let array3 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"];

for (var j = 0; j < 7; j++) {
    console.log(array3[j]);
}

// Soal 7
console.log(">==========Soal 7==========<");

let array4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

for (var i = 0; i < 10; i++) {
    if (array4[i] % 2 == 0) {
        console.log(array4[i]);
    }
}

// Soal 8
console.log(">==========Soal 8==========<");

let kalimat = ["saya", "sangat", "senang", "belajar", "javascript"];

var result = kalimat.join(" ");
console.log(result);

// Soal 9
console.log(">==========Soal 9==========<");

var sayuran = []

sayuran.push('Kangkung', 'Bayam', 'Buncis', 'Kubis', 'Timun', 'Seledri', 'Tauge');

for (var j = 0; j < 7; j++){
    console.log(sayuran[j]);
}
