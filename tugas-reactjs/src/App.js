import './App.css';
import Tugas7 from './07-tugas7/tugas7';
import Tugas8 from './08-tugas8/tugas8';

function App() {
  return (
    <div>
      <Tugas7 />
      <Tugas8 name="Hafiyyan Dwika Arya" email="iostream@gmail.com" batch="41" />
    </div>
  );
}

export default App;
