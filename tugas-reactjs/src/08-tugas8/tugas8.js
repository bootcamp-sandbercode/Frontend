import React from 'react'
import '../style.css'

const Tugas8 = (props) => {
    return (
        <div className="box">
            <h1>
                Data diri peserta kelas Reactjs
            </h1>
            <hr />
            <div className="content">
                <ul type="none">
                    <li>Nama Lengkap: {props.name}</li>
                    <li>Email: {props.email}</li>
                    <li>Batch Pelatihan: {props.batch}</li>
                </ul>
            </div>
        </div>
    );
}

export default Tugas8;
